 package org.example.product.service;

import org.example.product.entity.Product;
import org.example.product.repository.ProductInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService{

    private final ProductInterface productInterface;

    public ProductService(ProductInterface productInterface) {
        this.productInterface = productInterface;
    }
    public Product saveProduct(Product product){
        return productInterface.save(product);
    }
    public List<Product> saveAll(List<Product> products){
        return productInterface.saveAll(products);
    }
    public List<Product> getAllProduct(){
        return  productInterface.findAll();
    }
    public Product getProductById(int id){
        return productInterface.findById(id).orElse(null);
    }
    public Product getByProductName(String name){
        return  productInterface.findByName(name);
    }
    public String deleteById(int id){
        productInterface.deleteById(id);
        return "Data Deleted Successfully " + id;
    }
    public Product updatebyId(Product product){
        Product Existential = productInterface.findById(product.getId()).orElse(product);
        Existential.setName(product.getName());
        Existential.setPrice(product.getPrice());
        Existential.setQuantity(product.getQuantity());
        return productInterface.save(Existential);
    }
}
