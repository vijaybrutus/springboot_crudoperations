package org.example.product.controller;

import org.example.product.entity.Product;
import org.example.product.service.ProductService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping("/addProduct")
    public Product addProduct(@RequestBody Product product){
        return productService.saveProduct(product);
    }
    @PostMapping("/addProducts")
    public List<Product> addProducts(@RequestBody List<Product> products){
        return productService.saveAll(products);
    }
    @GetMapping("/findAllProduct")
    public List<Product> findAllProducts(){
        return productService.getAllProduct();
    }
    @GetMapping("/findProduct/{id}")
    public Product findByProductID(@PathVariable int id){
        return productService.getProductById(id);
    }
    @GetMapping("/findProductByName/{name}")
    public Product findByProductName(@PathVariable  String name){
        return productService.getByProductName(name);
    }
    @PutMapping("/updateProduct")
    public Product updateProduct(@RequestBody Product product){
        return productService.updatebyId(product);
    }
    @DeleteMapping("/deleteProduct/{id}")
    public  String deleteProduct(@PathVariable int id){
        return productService.deleteById(id);
    }

    
}
