package org.example.product.repository;

import org.example.product.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductInterface extends JpaRepository<Product, Integer> {
    Product findByName(String name);
}
